package com.example.greenthumb

data class Users(
    var user_id: Int,
    var email_id: String?,
    var user_type: String?
)
